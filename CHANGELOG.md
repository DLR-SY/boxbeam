# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [ [1.3.0](https://gitlab.com/dlr-sy/boxbeam/-/tags/v1.3.0) ] - 2024-07-22
### Changed
- Public release

## [ [1.2.0](https://gitlab.com/dlr-sy/boxbeam/-/tags/v1.2.0) ] - 2024-07-20
### Changed
- Fixing pipeline issues on windows
- Added documentation
- Added tests
- Added zenodo reference

## [ [1.1.0](https://gitlab.com/dlr-sy/boxbeam/-/tags/v1.1.0) ] - 2024-07-04
### Changed
- Preparing public release
- Creating push mirror to publish source code on gitlab.com

## [ [1.0.0](https://gitlab.dlr.de/fa_sw/boxbeam/-/tags/v1.0.0) ] - 2022-12-15
### Changed
- Added poetry support
- Added documentation
