---
name: Bug report
about: Template to report a problem with BoxBeam. Thank you for helping to resolve and improve the project.
---

<!--
Welcome!
Your issue may already be reported! Please search on the [issue tracker](../) for similar open and closed issues before creating one.
We kindly ask that you fill out the issue template below - not doing so needs a good reason.
-->
~"type::bug" ~"workflow::inreview"

## Summary

<!--
Summarize the bug encountered concisely
-->

## Expected behavior

<!-- Please describe in bullet points the expected behavior. -->

## Actual behavior

<!-- Please describe in bullet points the actual behavior. -->

## Steps to reproduce this issue?

<!-- Please describe in bullet points the steps to reproduce the behavior. This is very important. -->

## Screenshots, logs, error output, etc?

<!--
(Paste any relevant logs - please use code blocks (```) to format console output, logs, and code as it's very hard to read otherwise.)
(If the logs are too long, please paste to https://ghostbin.com/ and insert the link here.)
-->

## Environment

  - Python version: <!-- use `python -version` in the command line to find out the version -->
  - Operating system:  <!-- Windows, Linux, MacOS etc. -->
  
## MWE

<!-- Please provide a **minimal** working example and associated description.  -->

