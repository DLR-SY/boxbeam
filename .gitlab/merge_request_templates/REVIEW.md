---
name: Review
about: Merge request template for BoxBeam. Thank you for helping to improve the project.
---

<!--- Provide a general summary of your changes in the Title above -->
~"type::request" ~"workflow::inreview"

## Types of changes
<!--- What types of changes does your code introduce? Put an `x` in all the boxes that apply: -->
- [ ] Bug fix (non-breaking change which fixes an issue)
- [ ] New feature (non-breaking change which adds functionality)
- [ ] Breaking change (fix or feature that would cause existing functionality to not work as expected)

## Description
<!--- Describe your changes in detail -->

## Motivation and Context
<!--- Why is this change required? What problem does it solve? -->
<!--- If it fixes an open issue, please link to the issue here. -->

Fix #

## Quality Control

- [ ] I have added tests to cover my changes.
- [ ] All new and existing tests passed.

<!--- Please describe in detail how you tested your changes. -->
<!--- Include details of your testing environment, tests ran to see how -->
<!--- your change affects other areas of the code, etc. -->

## Screenshots (if appropriate):

## Checklist:
<!--- Go over all the following points, and put an `x` in all the boxes that apply. -->
<!--- If you're unsure about any of these, don't hesitate to ask. We're here to help! -->
- [ ] I have checked that there are no other open Merge Requests for the same update/change.
- [ ] My code follows the code style of this project.
- [ ] I have documented my code.
- [ ] My change requires a change to the written documentation.
- [ ] I have updated the documentation accordingly.
- [ ] I have provided application examples for the user.

In case existing code had to be modified on the way:
- [ ] I followed the pathfinder rule.
- [ ] I modified the code to follow the code style of this project.
- [ ] I completed the documentation or wrote documentation from scratch.
- [ ] I completed test coverage or implemented new tests from scratch.
