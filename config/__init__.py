# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#                               Boxbeam                          #
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"""
Top-level compatibility interface for Boxbeam as a standalone python package.
 
@note: Boxbeam
Created on 22.07.2024

@version: 1.0
----------------------------------------------------------------------------------------------
@requires:
       - 

@change: 
       -    
                           
@author: garb_ma                                                     [DLR-SY,STM Braunschweig]
----------------------------------------------------------------------------------------------
"""

## @package Boxbeam
# Top-level compatibility interface for Boxbeam as a standalone python package.
## @authors 
# Marc Garbade
## @date
# 07.22.2024
## @par Notes/Changes
# - Added documentation // mg 07.22.2024

import os, sys

# Add local path to global search path
sys.path.insert(0,os.path.dirname(__file__))

from _bbeam import *

if __name__ == '__main__':
    sys.exit()
    pass
