# -*- coding: utf-8 -*-
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %                 Build script for BoxBeam                     %
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"""
Build script to create a platfrom and Python dependent wheel.
 
@note: BoxBeam build script                 
Created on 12.12.2022    

@version:  1.0    
----------------------------------------------------------------------------------------------
@requires:
       - 

@change: 
       -    
   

@author: garb_ma                                                     [DLR-SY,STM Braunschweig]
----------------------------------------------------------------------------------------------
"""

import os, sys
import site
import subprocess
import tempfile
import shutil

def main():
    """
    This is the main entry point.
    """
    # Add internal build dependency resolver to current path
    site.addsitedir(site.getusersitepackages())
    # Import build tools
    try: from PyXMake import Command
    except: raise ImportError("Cannot import PyXMake. Skipping compilation")
    # Local import required for renaming operation
    from PyXMake.Tools import Utility
    # Define source and output directory
    source = os.path.join(os.getcwd(),"src")
    config = os.path.join(os.getcwd(),"config")
    output = os.path.join(os.getcwd(),"bin")
    # Define scratch folder
    scratch = tempfile.mkdtemp()
    # Create ouput directory 
    if not os.path.exists(os.path.join(output,"bbeam")): os.makedirs(os.path.join(output,"bbeam"))
    # This are all mandatory files for compilation
    files = Command.GetSourceCode(1)
    # Copy all required files to the output directory
    shutil.copy(os.path.join(config,"__init__.py"),os.path.join(output,"bbeam","__init__.py"))
    # Create a new alias file refering to old name
    Utility.ReplaceTextinFile(os.path.join(config,"__init__.py"), os.path.join(output,"boxbeam.py"),{"_bbeam":"bbeam"})
    # Execute build command
    Command.py2x("_bbeam", files=files, source=source, output=os.path.join(output,"bbeam"), scratch=scratch,
                  libs=[], include=[], dependency=[], incremental=False,
                  verbosity=2, no_arch=True, no_mkl=True)
    shutil.rmtree(scratch)
    pass

if __name__ == "__main__":
    main(); sys.exit()